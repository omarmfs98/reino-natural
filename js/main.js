
$( "#menu-1" ).on( "click", function() {
  activeMenu(1)
})

$( "#menu-2" ).on( "click", function() {
  activeMenu(2)
})

$( "#menu-3" ).on( "click", function() {
  activeMenu(3)
})

$( "#menu-4" ).on( "click", function() {
  activeMenu(4)
})

$( "#menu-5" ).on( "click", function() {
  activeMenu(5)
})

$( "#menu-6" ).on( "click", function() {
  activeMenu(6)
})

$( "#activity-one" ).on( "click", function() {
  checkActivityOne()
})

$( "#activity-two" ).on( "click", function() {
  checkActivityTwo()
})

$( "#evaluation-one" ).on( "click", function() {
  checkEvaluationOne()
})

$( "#evaluation-two" ).on( "click", function() {
  checkEvaluationTwo()
})

var word_correct = {
  activity1: {
    1: 'c',
    2: 'a',
    3: 'b',
    4: 'a',
    5: 'c'
  },
  activity2: {
    1: 'a',
    2: 'c',
    3: 'b',
    4: 'b',
    5: 'c',
    6: 'b'
  },
  evaluation1: {
    1: 'v',
    2: 'v',
    3: 'f',
    4: 'v',
    5: 'f',
  },
  evaluation2: {
    1: 'd',
    2: 'd',
    3: 'e',
    4: 'e',
    5: 'c',
    6: 'b',
    7: 'c',
    8: 'b'
  }
}

var word_user = {
  activity1: {
    1: '',
    2: '',
    3: '',
    4: '',
    5: ''
  },
  activity2: {
    1: '',
    2: '',
    3: '',
    4: '',
    5: '',
    6: ''
  },
  evaluation1: {
    1: '',
    2: '',
    3: '',
    4: '',
    5: '',
  },
  evaluation2: {
    1: '',
    2: '',
    3: '',
    4: '',
    5: '',
    6: '',
    7: '',
    8: ''
  }
}

var count = 0

function checkActivityOne() {
  for (var i = 1; i <= 5; i++) {
    word_user.activity1[i] = $('input[name=act1-' + i + ']:checked').val()
  }
  count = 0
  for (var i = 1; i <= 5; i++) {
    if (word_user.activity1[i] == word_correct.activity1[i]) {
      count++
    }
  }
  if (count == 5) {
    $('#info-activity-one').html('Haz respondido correctamente!')
  } else {
    $('#info-activity-one').html('Respondiste incorrectamente ' + (5 - count) + ' pregunta(s).')
  }
}

function checkActivityTwo() {
  for (var i = 1; i <= 6; i++) {
    word_user.activity2[i] = $('input[name=act2-' + i + ']:checked').val()
  }
  count = 0
  for (var i = 1; i <= 6; i++) {
    if (word_user.activity2[i] == word_correct.activity2[i]) {
      count++
    }
  }
  if (count == 6) {
    $('#info-activity-two').html('Haz respondido correctamente!')
  } else {
    $('#info-activity-two').html('Respondiste incorrectamente ' + (6 - count) + ' pregunta(s).')
  }
}

function checkEvaluationOne() {
  for (var i = 1; i <= 5; i++) {
    word_user.evaluation1[i] = $('input[name=eva1-' + i + ']:checked').val()
  }
  count = 0
  for (var i = 1; i <= 5; i++) {
    if (word_user.evaluation1[i] == word_correct.evaluation1[i]) {
      count++
    }
  }
  if (count == 5) {
    $('#info-evaluation-one').html('Haz respondido correctamente!')
  } else {
    $('#info-evaluation-one').html('Respondiste incorrectamente ' + (5 - count) + ' pregunta(s).')
  }
}

function checkEvaluationTwo() {
  for (var i = 1; i <= 8; i++) {
    word_user.evaluation2[i] = $('input[name=eva2-' + i + ']:checked').val()
  }
  count = 0
  for (var i = 1; i <= 8; i++) {
    if (word_user.evaluation2[i] == word_correct.evaluation2[i]) {
      count++
    }
  }
  if (count == 8) {
    $('#info-evaluation-two').html('Haz respondido correctamente!')
  } else {
    $('#info-evaluation-two').html('Respondiste incorrectamente ' + (8 - count) + ' pregunta(s).')
  }
}

function activeMenu(m) {
  switch (m) {
    case 1:
      showMenu(1)
      hideMenu(1)
      $('#menu-1').addClass('active')
      $('#menu-2').removeClass('active')
      $('#menu-3').removeClass('active')
      $('#menu-4').removeClass('active')
      $('#menu-5').removeClass('active')
      $('#menu-6').removeClass('active')
      // background
      $('.color-bg').addClass('btn-orange')
      $('.color-bg').removeClass('btn-purple')
      $('.color-bg').removeClass('btn-green')
      $('.color-bg').removeClass('btn-orange-darken')
      $('.color-bg').removeClass('btn-blue-light')
      $('.color-bg').removeClass('btn-rose')
      // border
      $('.border-main').addClass('border-orange')
      $('.border-main').removeClass('border-purple')
      $('.border-main').removeClass('border-green')
      $('.border-main').removeClass('border-orange-darken')
      $('.border-main').removeClass('border-blue-light')
      $('.border-main').removeClass('border-rose')
      break
    case 2:
      showMenu(2)
      hideMenu(2)
      $('#menu-1').removeClass('active')
      $('#menu-2').addClass('active')
      $('#menu-3').removeClass('active')
      $('#menu-4').removeClass('active')
      $('#menu-5').removeClass('active')
      $('#menu-6').removeClass('active')
      $('.color-bg').removeClass('btn-orange')
      $('.color-bg').addClass('btn-purple')
      $('.color-bg').removeClass('btn-green')
      $('.color-bg').removeClass('btn-orange-darken')
      $('.color-bg').removeClass('btn-blue-light')
      $('.color-bg').removeClass('btn-rose')
      // border
      $('.border-main').removeClass('border-orange')
      $('.border-main').addClass('border-purple')
      $('.border-main').removeClass('border-green')
      $('.border-main').removeClass('border-orange-darken')
      $('.border-main').removeClass('border-blue-light')
      $('.border-main').removeClass('border-rose')
      break
    case 3:
      showMenu(3)
      hideMenu(3)
      $('#menu-1').removeClass('active')
      $('#menu-2').removeClass('active')
      $('#menu-3').addClass('active')
      $('#menu-4').removeClass('active')
      $('#menu-5').removeClass('active')
      $('#menu-6').removeClass('active')
      $('.color-bg').removeClass('btn-orange')
      $('.color-bg').removeClass('btn-purple')
      $('.color-bg').addClass('btn-green')
      $('.color-bg').removeClass('btn-orange-darken')
      $('.color-bg').removeClass('btn-blue-light')
      $('.color-bg').removeClass('btn-rose')
      // border
      $('.border-main').removeClass('border-orange')
      $('.border-main').removeClass('border-purple')
      $('.border-main').addClass('border-green')
      $('.border-main').removeClass('border-orange-darken')
      $('.border-main').removeClass('border-blue-light')
      $('.border-main').removeClass('border-rose')
      break
    case 4:
      showMenu(4)
      hideMenu(4)
      $('#menu-1').removeClass('active')
      $('#menu-2').removeClass('active')
      $('#menu-3').removeClass('active')
      $('#menu-4').addClass('active')
      $('#menu-5').removeClass('active')
      $('#menu-6').removeClass('active')
      $('.color-bg').removeClass('btn-orange')
      $('.color-bg').removeClass('btn-purple')
      $('.color-bg').removeClass('btn-green')
      $('.color-bg').addClass('btn-orange-darken')
      $('.color-bg').removeClass('btn-blue-light')
      $('.color-bg').removeClass('btn-rose')
      // border
      $('.border-main').removeClass('border-orange')
      $('.border-main').removeClass('border-purple')
      $('.border-main').removeClass('border-green')
      $('.border-main').addClass('border-orange-darken')
      $('.border-main').removeClass('border-blue-light')
      $('.border-main').removeClass('border-rose')
      break
    case 5:
      showMenu(5)
      hideMenu(5)
      $('#menu-1').removeClass('active')
      $('#menu-2').removeClass('active')
      $('#menu-3').removeClass('active')
      $('#menu-4').removeClass('active')
      $('#menu-5').addClass('active')
      $('#menu-6').removeClass('active')
      $('.color-bg').removeClass('btn-orange')
      $('.color-bg').removeClass('btn-purple')
      $('.color-bg').removeClass('btn-green')
      $('.color-bg').removeClass('btn-orange-darken')
      $('.color-bg').addClass('btn-blue-light')
      $('.color-bg').removeClass('btn-rose')
      // border
      $('.border-main').removeClass('border-orange')
      $('.border-main').removeClass('border-purple')
      $('.border-main').removeClass('border-green')
      $('.border-main').removeClass('border-orange-darken')
      $('.border-main').addClass('border-blue-light')
      $('.border-main').removeClass('border-rose')
      break
    default:
      showMenu(6)
      hideMenu(6)
      $('#menu-1').removeClass('active')
      $('#menu-2').removeClass('active')
      $('#menu-3').removeClass('active')
      $('#menu-4').removeClass('active')
      $('#menu-5').removeClass('active')
      $('#menu-6').addClass('active')
      $('.color-bg').removeClass('btn-orange')
      $('.color-bg').removeClass('btn-purple')
      $('.color-bg').removeClass('btn-green')
      $('.color-bg').removeClass('btn-orange-darken')
      $('.color-bg').removeClass('btn-blue-light')
      $('.color-bg').addClass('btn-rose')
      // border
      $('.border-main').removeClass('border-orange')
      $('.border-main').removeClass('border-purple')
      $('.border-main').removeClass('border-green')
      $('.border-main').removeClass('border-orange-darken')
      $('.border-main').removeClass('border-blue-light')
      $('.border-main').addClass('border-rose')
      break
  }
}

function showMenu(m) {
  switch (m) {
    case 1:
      $('#section1').removeClass('hide')
      break
    case 2:
      $('#section2').removeClass('hide')
      break
    case 3:
      $('#section3').removeClass('hide')
      break
    case 4:
      $('#section4').removeClass('hide')
      break
    case 5:
      $('#section5').removeClass('hide')
      break
    default:
      $('#section6').removeClass('hide')
      break
  }
}

function hideMenu(m) {
  switch (m) {
    case 1:
      $('#section2').addClass('hide')
      $('#section3').addClass('hide')
      $('#section4').addClass('hide')
      $('#section5').addClass('hide')
      $('#section6').addClass('hide')
      break
    case 2:
      $('#section1').addClass('hide')
      $('#section3').addClass('hide')
      $('#section4').addClass('hide')
      $('#section5').addClass('hide')
      $('#section6').addClass('hide')
      break
    case 3:
      $('#section1').addClass('hide')
      $('#section2').addClass('hide')
      $('#section4').addClass('hide')
      $('#section5').addClass('hide')
      $('#section6').addClass('hide')
      break
    case 4:
      $('#section1').addClass('hide')
      $('#section2').addClass('hide')
      $('#section3').addClass('hide')
      $('#section5').addClass('hide')
      $('#section6').addClass('hide')
      break
    case 5:
      $('#section1').addClass('hide')
      $('#section2').addClass('hide')
      $('#section3').addClass('hide')
      $('#section4').addClass('hide')
      $('#section6').addClass('hide')
      break
    default:
      $('#section1').addClass('hide')
      $('#section2').addClass('hide')
      $('#section3').addClass('hide')
      $('#section4').addClass('hide')
      $('#section5').addClass('hide')
      break
  }
}
